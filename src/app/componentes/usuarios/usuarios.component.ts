import { Component, OnInit } from '@angular/core';
import {ImagenesService} from "../../servicio/imagenes.service";
@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  lista = []; 
  autores = []
  constructor(private imagenservice : ImagenesService) { }

  ngOnInit() {

    this.imagenservice.getImagenes().subscribe(
      res => {
        console.log(res)
         this.lista = [res]; 
      },
      err =>console.log(err)
    )

    this.imagenservice.getAutor().subscribe(
      res =>{
        console.log(res)

        this.autores.push(res)
      },
      err => console.log(err)
    )

  }

}
