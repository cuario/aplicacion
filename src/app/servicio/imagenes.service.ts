import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'


@Injectable({
  providedIn: 'root'
})
export class ImagenesService {

  URL = 'http://fakerestapi.azurewebsites.net/api/v1/Activities';

  url2 = "http://fakerestapi.azurewebsites.net/api/v1/Authors";
  constructor(private http : HttpClient) { }


  getImagenes(){
    return this.http.get(`${this.URL}`);
  }

  getAutor(){
    return this.http.get(this.url2);
  }

  

}
